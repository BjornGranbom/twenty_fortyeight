﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TwentyFortyEightGame;

namespace TwenyFortyEightWindow
{
    public partial class Form1 : Form
    {
        private readonly TwentyFortyEightGame.TwentyFortyEightGame twentyFortyEightGame;
        private Label[,] squares;

        public Form1()
        {
            InitializeComponent();
            twentyFortyEightGame = new TwentyFortyEightGame.TwentyFortyEightGame();
            InitBoard();
            WriteBoard();
        }

        private void WriteBoard()
        {
            var board = twentyFortyEightGame.GetBoard();
            for (int x = 0; x < 4; x++)
            {
                for (int y = 0; y < 4; y++)
                {
                    if (board[x, y] == 0)
                    {
                        squares[x, y].Text = "";
                        squares[x, y].BackColor = Color.LightGray;
                        squares[x, y].ForeColor = Color.Black;
                    }
                    else
                    {
                        squares[x, y].Text = board[x, y].ToString();
                        squares[x, y].BackColor = GetBackColor(board[x,y]);
                        squares[x, y].ForeColor = GetForeColor(board[x, y]);
                    }
                }
            }
            var f = new NumberFormatInfo { NumberGroupSeparator = " " };
            Score.Text="Score: " + twentyFortyEightGame.GetScore().ToString("#,0", f);
        }

        private Color GetForeColor(uint v)
        {
            switch(v)
            {
                default:
                    return Color.White;
                case 2:
                case 4:
                case 8:
                case 16:
                case 32:
                case 256:
                    return Color.White;
                case 64:
                case 128:
                case 512:
                case 1024:
                case 2048:
                    return Color.Black;
            }
        }

        private Color GetBackColor(uint v)
        {
            switch (v)
            {
                default:
                    return Color.Black;
                case 2:
                    return Color.FromArgb(100, 100, 0);
                case 4:
                    return Color.DarkGreen;
                case 8:
                    return Color.DarkBlue;
                case 16:
                    return Color.DarkRed;
                case 32:
                    return Color.DarkCyan;
                case 64:
                    return Color.Yellow;
                case 128:
                    return Color.LightGreen;
                case 256:
                    return Color.Blue;
                case 512:
                    return Color.Red;
                case 1024:
                    return Color.Cyan;
                case 2048:
                    return Color.White;
            }
        }

        private void InitBoard()
        {
            squares = new Label[4, 4];
            for (int x=0;x<4;x++)
            {
                for (int y=0;y<4;y++)
                {
                    squares[x, y] = new Label
                    {
                        Text = "",
                        BackColor = Color.LightGray,
                        ForeColor = Color.Black,
                        BorderStyle = BorderStyle.Fixed3D,
                        TextAlign = ContentAlignment.MiddleCenter,
                        AutoSize = false,
                        Size = new Size { Width = 65, Height = 65 },
                        Location = new Point { X = x * 70 + 5, Y = y * 70 + 5 },
                        Font = new System.Drawing.Font("Segoe UI", 17F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point)
                };
                    this.Controls.Add(squares[x, y]);
                }
            }
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.W:
                case Keys.Up:
                    if (twentyFortyEightGame.MoveUp()) GameEnded();
                    break;
                case Keys.A:
                case Keys.Left:
                    if (twentyFortyEightGame.MoveLeft()) GameEnded();
                    break;
                case Keys.S:
                case Keys.Down:
                    if (twentyFortyEightGame.MoveDown()) GameEnded();
                    break;
                case Keys.D:
                case Keys.Right:
                    if (twentyFortyEightGame.MoveRight()) GameEnded();
                    break;
            }
            WriteBoard();
        }

        private void GameEnded()
        {
            WriteBoard();
            this.Update();
            MessageBox.Show("No more moves");
        }
    }
    public static class ControlExtensions
    {
        public static T Clone<T>(this T controlToClone)
            where T : Control
        {
            PropertyInfo[] controlProperties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

            T instance = Activator.CreateInstance<T>();

            foreach (PropertyInfo propInfo in controlProperties)
            {
                if (propInfo.CanWrite)
                {
                    if (propInfo.Name != "WindowTarget")
                        propInfo.SetValue(instance, propInfo.GetValue(controlToClone, null), null);
                }
            }

            return instance;
        }
    }

}
