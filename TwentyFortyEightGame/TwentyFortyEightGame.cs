﻿using System;

namespace TwentyFortyEightGame
{
    public class TwentyFortyEightGame
    {
        private UInt32[,] _board;

        public TwentyFortyEightGame()
        {
            CreateBoard();
            StartGame();

        }

        public void StartGame()
        {
            for (int x = 0; x < 4; x++)
            {
                for(int y = 0; y < 4; y++)
                {
                    _board[x, y] = 0;
                }
            }
            int n = 0;
            while (n<2)
            {
                var coordinate = RandomPos();
                if (_board[coordinate.X,coordinate.Y]==0)
                {
                    _board[coordinate.X, coordinate.Y] = 2;
                    n++;
                }
            }
        }

        public UInt32[,] GetBoard()
        {
            return _board;
        }

        public bool MoveDown()
        //Returns true if not possiblie to move any more in any direction
        {
            bool newNumber = false;
            for (int x = 0; x < 4; x++)
            {
                if (MoveAndJoinDownColumn(x))
                {
                    newNumber = true;
                }
            }
            if (newNumber) PlaceNewRandomPlace();
            return CheckForEnd();
        }

        public bool MoveUp()
            //Returns true if not possiblie to move any more in any direction
        {
            bool newNumber = false;
            for (int x=0;x<4;x++)
            {
                if (MoveAndJoinUpColumn(x))
                {
                    newNumber = true;
                }
            }
            if (newNumber) PlaceNewRandomPlace();
            return CheckForEnd();
        }

        public bool MoveLeft()
        //Returns true if not possiblie to move any more in any direction
        {
            bool newNumber = false;
            for (int y = 0; y < 4; y++)
            {
                if (MoveAndJoinLeftColumn(y))
                {
                    newNumber = true;
                }
            }
            if (newNumber) PlaceNewRandomPlace();
            return CheckForEnd();
        }

        public bool MoveRight()
        //Returns true if not possiblie to move any more in any direction
        {
            bool newNumber = false;
            for (int y = 0; y < 4; y++)
            {
                if (MoveAndJoinRightColumn(y))
                {
                    newNumber = true;
                }
            }
            if (newNumber) PlaceNewRandomPlace();
            return CheckForEnd();
        }

        public UInt32 GetScore()
        {
            UInt32 score = 0;
            for (int x = 0; x < 4; x++)
            {
                for (int y = 0; y < 4; y++)
                {
                    score += _board[x, y] * _board[x, y];
                }
            }
            return score;
        }
        private void PlaceNewRandomPlace()
        {
            if (!CheckIfBoardIsFull())
            {
                var number = GetNewRandomNumber();
                Coordinate coordinate;
                do
                {
                    coordinate = RandomPos();
                } while (_board[coordinate.X, coordinate.Y] != 0);
                _board[coordinate.X, coordinate.Y] = number;
            }
        }

        private UInt32 GetNewRandomNumber()
        {
            var r = new Random();
            switch(r.Next(0,4))
            {
                default:
                case 0:
                case 1:
                case 2:
                    return 2;
                case 3:
                    return 4;
            }
        }

        private bool CheckForEnd()
        {
            return (CheckIfBoardIsFull() && CheckIfNotPossibleToJoin());
        }

        private bool CheckIfNotPossibleToJoin()
        {
            for (int x = 0; x < 4; x++)
            {
                if (CheckIfPossibleToJoinColumn(x)) return false;
            }
            for (int y = 0; y < 4; y++)
            {
                if (CheckIfPossibleToJoinRow(y)) return false;
            }
            return true;
        }

        private bool CheckIfPossibleToJoinRow(int y)
        {
            for (int x = 0; x < 3; x++)
            {
                if (_board[x, y] == _board[x + 1, y]) return true;
            }
            return false;
        }

        private bool CheckIfPossibleToJoinColumn(int x)
        {
            for (int y = 0; y < 3; y++)
            {
                if (_board[x,y]==_board[x,y+1]) return true;
            }
            return false;
        }

        private bool CheckIfBoardIsFull()
        {
            for (int x = 0; x < 4; x++)
            {
                for (int y = 0; y < 4; y++)
                {
                    if (_board[x, y] == 0) return false;
                }
            }
            return true;
        }

        private bool MoveAndJoinUpColumn(int x)
        {
            bool retur = false;
            if (MoveUpColumn(x)) retur = true;
            if (JoinUpColumn(x)) retur = true;
            if (MoveUpColumn(x)) retur = true;
            return retur;
        }

        private bool JoinUpColumn(int x)
        {
            bool retur = false;
            for (int y = 1; y < 4; y++)
            {
                if (_board[x, y] > 0)
                {
                    if (_board[x, y - 1] == _board[x, y])
                    {
                        _board[x, y - 1] += _board[x, y];
                        _board[x, y] = 0;
                        //y--;
                        retur = true;
                    }
                }
            }
            return retur;
        }

        private bool MoveUpColumn(int x)
        {
            bool retur = false;
            for (int y = 1; y < 4; y++)
            {
                if (_board[x, y] > 0)
                {
                    if (_board[x, y - 1] == 0)
                    {
                        _board[x, y - 1] = _board[x, y];
                        _board[x, y] = 0;
                        y -= 2;
                        if (y < 0) y = 0;
                        retur = true;
                        continue;
                    }
                }
            }
            return retur;
        }

        private bool MoveAndJoinDownColumn(int x)
        {
            bool retur = false; ;
            if (MoveDownColumn(x)) retur = true;
            if (JoinDownColumn(x)) retur = true;
            if (MoveDownColumn(x)) retur = true;
            return retur;
        }

        private bool JoinDownColumn(int x)
        {
            bool retur = false; ;
            for (int y = 2; y >= 0; y--)
            {
                if (_board[x, y] > 0)
                {
                    if (_board[x, y + 1] == _board[x, y])
                    {
                        _board[x, y + 1] += _board[x, y];
                        _board[x, y] = 0;
                        //y++;
                        retur = true;
                    }
                }
            }
            return retur;
        }

        private bool MoveDownColumn(int x)
        {
            bool retur = false; ;
            for (int y = 2; y >= 0; y--)
            {
                if (_board[x, y] > 0)
                {
                    if (_board[x, y + 1] == 0)
                    {
                        _board[x, y + 1] = _board[x, y];
                        _board[x, y] = 0;
                        y += 2;
                        if (y > 3) y = 3;
                        retur = true;
                        continue;
                    }
                }
            }
            return retur;
        }

        private bool MoveAndJoinLeftColumn(int y)
        {
            bool retur = false; ;
            if (MoveLeftColumn(y)) retur = true;
            if (JoinLeftColumn(y)) retur = true;
            if (MoveLeftColumn(y)) retur = true;
            return retur;
        }

        private bool JoinLeftColumn(int y)
        {
            bool retur = false; ;
            for (int x = 1; x < 4; x++)
            {
                if (_board[x, y] > 0)
                {
                    if (_board[x - 1, y] == _board[x, y])
                    {
                        _board[x - 1, y] += _board[x, y];
                        _board[x, y] = 0;
                        //x--;
                        retur = true;
                    }
                }
            }
            return retur;
        }

        private bool MoveLeftColumn(int y)
        {
            bool retur = false; ;
            for (int x = 1; x < 4; x++)
            {
                if (_board[x, y] > 0)
                {
                    if (_board[x - 1, y] == 0)
                    {
                        _board[x - 1, y] = _board[x, y];
                        _board[x, y] = 0;
                        x -= 2;
                        if (x < 0) x = 0;
                        retur = true;
                        continue;
                    }
                }
            }
            return retur;
        }

        private bool MoveAndJoinRightColumn(int y)
        {
            bool retur = false; ;
            if (MoveRightColumn(y)) retur = true;
            if (JoinRightColumn(y)) retur = true;
            if (MoveRightColumn(y)) retur = true;
            return retur;
        }

        private bool JoinRightColumn(int y)
        {
            bool retur = false; ;
            for (int x = 2; x >= 0; x--)
            {
                if (_board[x, y] > 0)
                {
                    if (_board[x + 1, y] == _board[x, y])
                    {
                        _board[x + 1, y] += _board[x, y];
                        _board[x, y] = 0;
                        //x++;
                        retur = true;
                    }
                }
            }
            return retur;
        }

        private bool MoveRightColumn(int y)
        {
            bool retur = false; ;
            for (int x = 2; x >= 0; x--)
            {
                if (_board[x, y] > 0)
                {
                    if (_board[x + 1, y] == 0)
                    {
                        _board[x + 1, y] = _board[x, y];
                        _board[x, y] = 0;
                        x += 2;
                        if (x > 3) x = 3;
                        retur = true;
                        continue;
                    }
                }
            }
            return retur;
        }

        private Coordinate RandomPos()
        {
            var coordinate = new Coordinate();
            var r = new Random();
            coordinate.X = r.Next(0, 4);
            coordinate.Y = r.Next(0, 4);
            return coordinate;
        }

        private void CreateBoard()
        {
            _board = new UInt32[4,4];
        }

        private class Coordinate
        {
            public int X { get; set; }
            public int Y { get; set; }
        }
    }
}
