﻿using System;

namespace TwentyFortyEightWeb.Models
{
    public interface IGame3
    {
        Guid CreateNewInstance();
        TwentyFortyEightGame.TwentyFortyEightGame GetTwentFortyEight(Guid user_id);
    }
}