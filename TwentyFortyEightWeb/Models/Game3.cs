﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TwentyFortyEightWeb.Models
{
    public class Game3 : IGame3
    {
        private readonly List<GameInstanses> _ListInstanses;

        public Game3()
        {
            _ListInstanses = new List<GameInstanses>();
        }

        public TwentyFortyEightGame.TwentyFortyEightGame GetTwentFortyEight(Guid user_id)
        {
            TwentyFortyEightGame.TwentyFortyEightGame twentyFortyEight = _ListInstanses.FirstOrDefault(e => e.User_id == user_id).GetTwentFortyEight();
            return twentyFortyEight;
        }

        public Guid CreateNewInstance()
        {
            GameInstanses instanse = new GameInstanses();
            _ListInstanses.Add(instanse);
            return instanse.User_id;
        }
    }
}
