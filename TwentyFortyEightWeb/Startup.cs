using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using TwentyFortyEightWeb.Models;
using Serilog;

namespace TwentyFortyEightWeb
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddMvc();
            services.AddMvc(options => options.EnableEndpointRouting = false);
            //services.AddScoped(TwentyFortyEightGame.TwentyFortyEightGame);
            services.AddSingleton<TwentyFortyEightGame.TwentyFortyEightGame, TwentyFortyEightGame.TwentyFortyEightGame>();
            services.AddSingleton<IGame3, Game3>();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILogger<Startup> logging)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.Use(async (context,next)  =>
            {
                //logging.LogInformation(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + " New requast for " + context.Request.Path + " from " + context.Connection.RemoteIpAddress);
                logging.LogWarning(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + " New requast for " + context.Request.Path + " from " + context.Connection.RemoteIpAddress);
                //logging.LogError(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + " New requast for " + context.Request.Path + " from " + context.Connection.RemoteIpAddress);
                //logging.LogCritical(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + " New requast for " + context.Request.Path + " from " + context.Connection.RemoteIpAddress);
                //logging.LogDebug(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + " New requast for " + context.Request.Path + " from " + context.Connection.RemoteIpAddress);
                await next();
                logging.LogWarning(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + " Answere requast for " + context.Request.Path + " from " + context.Connection.RemoteIpAddress);
            });

            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseMvc();

            /*
            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync(System.Diagnostics.Process.GetCurrentProcess().ProcessName);
                });
            });
            */
        }
    }
}
