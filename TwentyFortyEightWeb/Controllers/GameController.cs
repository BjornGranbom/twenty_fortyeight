﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace TwentyFortyEightWeb.Controllers
{
    //[Route("~/")]
    [Route("~/[Controller]/[action]")]
    public class GameController : Controller
    {
        private TwentyFortyEightGame.TwentyFortyEightGame _TwentFortyEight;

        public GameController(TwentyFortyEightGame.TwentyFortyEightGame TwentFortyEight)
        {
            _TwentFortyEight = TwentFortyEight;
        }

        [Route("~/[Controller]")]
        [Route("~/[Controller]/Index")]
        public IActionResult Index()
        {
            //_TwentFortyEight = new TwentyFortyEightGame.TwentyFortyEightGame();
            return View();
        }

        public string Test()
        {
            return "GameController/Test()";
        }

        public ActionResult _game(int id=0)
        {
            //if (_TwentFortyEight == null) _TwentFortyEight = new TwentyFortyEightGame.TwentyFortyEightGame();
            if (id==0)
            {
                _TwentFortyEight.StartGame();
            }
            else
            {
                switch (id)
                {
                    case 1:
                        _TwentFortyEight.MoveUp();
                        break;
                    case 2:
                        _TwentFortyEight.MoveDown();
                        break;
                    case 3:
                        _TwentFortyEight.MoveLeft();
                        break;
                    case 4:
                        _TwentFortyEight.MoveRight();
                        break;
                }
            }
            var board = _TwentFortyEight.GetBoard();
            return PartialView(board);
        }
    }
}
