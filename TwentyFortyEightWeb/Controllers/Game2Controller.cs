﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace TwentyFortyEightWeb.Controllers
{
    //[Route("~/[action]")]
    [Route("~/[Controller]/[action]")]
    public class Game2Controller : Controller
    {
        private TwentyFortyEightGame.TwentyFortyEightGame _TwentFortyEight;

        public Game2Controller(TwentyFortyEightGame.TwentyFortyEightGame TwentFortyEight)
        {
            _TwentFortyEight = TwentFortyEight;
        }

        //[Route("~/", Name = "Default")]
        [Route("~/[Controller]")]
        [Route("~/[Controller]/Index")]
        public IActionResult Index()
        {

            return View();
        }

        public class Outputdata
        {
            public  class inneroutputdata
            {
                public Dictionary<string, uint> Value;
                public inneroutputdata()
                {
                    Value = new Dictionary<string, uint>();
                }
            }
            public Dictionary<string, inneroutputdata> Value;
            public Outputdata()
            {
                Value = new Dictionary<string, inneroutputdata>();
            }

            public Outputdata assign(uint[,] data)
            {
                string[] convert = { "one", "two", "tree", "four" };
                for (uint x=0;x<4;x++)
                {
                    inneroutputdata i = new inneroutputdata();
                    for (uint y=0;y<4;y++)
                    {
                        i.Value[convert[y]] = data[x, y];
                    }
                    this.Value[convert[x]] = i;
                }
                return this;
            }
        }
        public uint[] _game(int id = 0)
        {
            switch (id)
            {
                case 1:
                    _TwentFortyEight.MoveUp();
                    break;
                case 2:
                    _TwentFortyEight.MoveDown();
                    break;
                case 3:
                    _TwentFortyEight.MoveLeft();
                    break;
                case 4:
                    _TwentFortyEight.MoveRight();
                    break;
            }
            var board = _TwentFortyEight.GetBoard();
            //var outputdata = new Outputdata();
            //outputdata.assign(board);
            //var jsonBoard= Json(outputdata);
            //return jsonBoard;
            var data = new uint[16];
            for (uint x = 0; x < 4; x++)
            {
                for (uint y = 0; y < 4; y++)
                {
                    data[4 * x + y] = board[x, y];
                }
            }
            return data;
        }
    }
}
