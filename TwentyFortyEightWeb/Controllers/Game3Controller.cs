﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TwentyFortyEightWeb.Models;

namespace TwentyFortyEightWeb.Controllers
{
    [Route("~/[action]")]
    [Route("~/[Controller]/[action]")]
    public class Game3Controller : Controller
    {
        private IGame3 _game3;

        public Game3Controller(IGame3 game3)
        {
            _game3 = game3;
        }

        [Route("~/", Name = "Default")]
        [Route("~/[Controller]")]
        [Route("~/[Controller]/Index")]
        public IActionResult Index()
        {

            return View();
        }


        public uint[] _game(int id = 0)
        {
            TwentyFortyEightGame.TwentyFortyEightGame _TwentFortyEight;
            if (HttpContext.Request.Cookies["Game2048UserId"] == null)
            {
                CookieOptions cookieOptions = new CookieOptions();
                cookieOptions.Expires = new DateTimeOffset(DateTime.Now.AddDays(1));
                var game = _game3.CreateNewInstance();
                HttpContext.Response.Cookies.Append("Game2048UserId", game.ToString() , cookieOptions);
                _TwentFortyEight = _game3.GetTwentFortyEight(game);
            }
            else
            {
                var cookie = HttpContext.Request.Cookies["Game2048UserId"];
                try
                {
                    _TwentFortyEight = _game3.GetTwentFortyEight(new Guid(cookie));
                }
                catch
                {
                    HttpContext.Response.Cookies.Delete("Game2048UserId");
                    CookieOptions cookieOptions = new CookieOptions();
                    cookieOptions.Expires = new DateTimeOffset(DateTime.Now.AddDays(1));
                    var game = _game3.CreateNewInstance();
                    HttpContext.Response.Cookies.Append("Game2048UserId", game.ToString(), cookieOptions);
                    _TwentFortyEight = _game3.GetTwentFortyEight(game);
                }
            }

            switch (id)
            {
                case 0:
                    _TwentFortyEight.StartGame();
                    break;
                case 1:
                    _TwentFortyEight.MoveUp();
                    break;
                case 2:
                    _TwentFortyEight.MoveDown();
                    break;
                case 3:
                    _TwentFortyEight.MoveLeft();
                    break;
                case 4:
                    _TwentFortyEight.MoveRight();
                    break;
            }
            var board = _TwentFortyEight.GetBoard();
            //var outputdata = new Outputdata();
            //outputdata.assign(board);
            //var jsonBoard= Json(outputdata);
            //return jsonBoard;
            var data = new uint[17];
            for (uint x = 0; x < 4; x++)
            {
                for (uint y = 0; y < 4; y++)
                {
                    data[4 * x + y] = board[x, y];
                }
            }
            data[16] = _TwentFortyEight.GetScore();
            return data;
        }
    }
}
