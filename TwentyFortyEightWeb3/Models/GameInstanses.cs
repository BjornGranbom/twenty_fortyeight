﻿using System;

namespace TwentyFortyEightWeb3.Models
{
    internal class GameInstanses
    {
        public GameInstanses()
        {
            User_id = Guid.NewGuid();
            _TwentFortyEight = new TwentyFortyEightGame.TwentyFortyEightGame();
            Expire = DateTime.Now.AddDays(1);
        }

        public Guid User_id { get; internal set; }
        private TwentyFortyEightGame.TwentyFortyEightGame _TwentFortyEight;
        public DateTime Expire { get; private set; }

        internal TwentyFortyEightGame.TwentyFortyEightGame GetTwentFortyEight()
        {
            return _TwentFortyEight;
        }
    }
}