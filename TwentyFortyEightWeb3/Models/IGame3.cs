﻿using System;

namespace TwentyFortyEightWeb3.Models
{
    public interface IGame3
    {
        Guid CreateNewInstance();
        TwentyFortyEightGame.TwentyFortyEightGame GetTwentFortyEight(Guid user_id);
    }
}