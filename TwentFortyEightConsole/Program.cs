﻿using System;
using System.Globalization;
using TwentyFortyEightGame;

namespace TwentFortyEightConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            var twentyFortyEightGame = new TwentyFortyEightGame.TwentyFortyEightGame();

            bool finnish = false;
            do
            {
                WriteBoardToConsole(twentyFortyEightGame.GetBoard(), twentyFortyEightGame.GetScore());
                var key = Console.ReadKey().Key;
                //var key = SimulateKey();
                switch(key)
                {
                    case ConsoleKey.W:
                    case ConsoleKey.UpArrow:
                        if (twentyFortyEightGame.MoveUp()) finnish = true;
                        break;
                    case ConsoleKey.A:
                    case ConsoleKey.LeftArrow:
                        if (twentyFortyEightGame.MoveLeft()) finnish = true;
                        break;
                    case ConsoleKey.S:
                    case ConsoleKey.DownArrow:
                        if (twentyFortyEightGame.MoveDown()) finnish = true;
                        break;
                    case ConsoleKey.D:
                    case ConsoleKey.RightArrow:
                        if (twentyFortyEightGame.MoveRight()) finnish = true;
                        break;
                }
            } while (!finnish);
            WriteBoardToConsole(twentyFortyEightGame.GetBoard(), twentyFortyEightGame.GetScore());
        }

        private static ConsoleKey SimulateKey()
        {
            var r = new Random();
            int a = r.Next(1, 5);
            switch (a)
            {
                case 1:
                    return ConsoleKey.UpArrow;
                case 2:
                    return ConsoleKey.LeftArrow;
                case 3:
                    return ConsoleKey.DownArrow;
                case 4:
                    return ConsoleKey.RightArrow;
            }
            throw new IndexOutOfRangeException();
         }

        private static void WriteBoardToConsole(uint[,] board, UInt32 score)
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Clear();
            for(int y=0;y<4; y++)
            {
                WriteLine();
                for (int x = 0; x < 4; x++)
                {
                    WriteColumnLine();
                    WriteNumber(board[x, y]);
                }
                WriteColumnLine();
                Console.WriteLine();
            }
            WriteLine();
            WriteScore(score);
        }

        private static void WriteScore(uint score)
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine();
            var f = new NumberFormatInfo { NumberGroupSeparator = " " };
            Console.WriteLine("Score: " + score.ToString("#,0", f));
            Console.ForegroundColor = ConsoleColor.White;
        }

        private static void WriteColumnLine()
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Write("|");
        }

        private static void WriteNumber(uint v)
        {
            const int length = 7;
            if (v==0)
            {
                Console.Write(new string(' ', length));
            }
            else
            {
                ChangeColor(v);
                int numberlength = v.ToString().Length;
                int before = (length - numberlength) / 2;
                int after = length - numberlength - before;
                Console.Write(new string(' ', before));
                Console.Write(v);
                Console.Write(new string(' ', after));
                Console.ForegroundColor = ConsoleColor.White;
            }
        }

        private static void WriteLine()
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine("---------------------------------");
        }

        private static void ChangeColor(uint v)
        {
            switch(v)
            {
                default:
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                case 2:
                    Console.BackgroundColor = ConsoleColor.DarkYellow;
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                case 4:
                    Console.BackgroundColor = ConsoleColor.DarkGreen;
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                case 8:
                    Console.BackgroundColor = ConsoleColor.DarkBlue;
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                case 16:
                    Console.BackgroundColor = ConsoleColor.DarkRed;
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                case 32:
                    Console.BackgroundColor = ConsoleColor.DarkCyan;
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                case 64:
                    Console.BackgroundColor = ConsoleColor.Yellow;
                    Console.ForegroundColor = ConsoleColor.Black;
                    break;
                case 128:
                    Console.BackgroundColor = ConsoleColor.Green;
                    Console.ForegroundColor = ConsoleColor.Black;
                    break;
                case 256:
                    Console.BackgroundColor = ConsoleColor.Blue;
                    Console.ForegroundColor = ConsoleColor.Black;
                    break;
                case 512:
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.ForegroundColor = ConsoleColor.Black;
                    break;
                case 1024:
                    Console.BackgroundColor = ConsoleColor.Cyan;
                    Console.ForegroundColor = ConsoleColor.Black;
                    break;
                case 2048:
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.ForegroundColor = ConsoleColor.Black;
                    break;
            }
        }
    }
}
